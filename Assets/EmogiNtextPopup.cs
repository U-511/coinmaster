﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmogiNtextPopup : MonoBehaviour
{
    public List<string> textPull;
    public List<Sprite> spritePull;
    [Space]
    public Vector2 percentBounds; // 0 - 1 [.00]
    public float maxRotationAngle;
    [Space]
    public float fadingTime;
    [Space]
    public GameObject textPref;
    public GameObject imagePref;
    [Space]
    public GameObject mainCanvas;

    public static EmogiNtextPopup singleton;

    private void Awake()
    {
        singleton = this;
    }

    /// <summary>
    /// Spawns cheering text in screen bounds, take random text from text pull
    /// </summary>
    public void CheeringTextSpawn()
    {
        float rndX = Random.Range(Screen.width * percentBounds.x, Screen.width * (1 - percentBounds.x));
        float rndY = Random.Range(Screen.height * percentBounds.y, Screen.height * (1 - percentBounds.y));
        Vector2 screenSpawn = new Vector2(rndX, rndY);

        float rndRotation = 0;
        if (maxRotationAngle != 0)
            rndRotation = Random.Range(-maxRotationAngle, maxRotationAngle);

        GameObject cheerText = Instantiate(textPref, screenSpawn, Quaternion.Euler(0, 0, rndRotation), mainCanvas.transform);
        cheerText.GetComponent<Text>().text = textPull[Random.Range(0, textPull.Count)];

        FadingDestroyCall(cheerText, fadingTime);
        
    }

    /// <summary>
    /// Spawns Image emogy in screen bounds, take rnd sprite from spritePull
    /// </summary>
    public void EmogiSpawn()
    {
//        float rndX = Random.Range(Screen.width * percentBounds.x, Screen.width * (1 - percentBounds.x));
//        float rndY = Random.Range(Screen.height * percentBounds.y, Screen.height * (1 - percentBounds.y));
//        Vector2 screenSpawn = new Vector2(rndX, rndY);
//
//        float rndRotation = 0;
//        if (maxRotationAngle != 0)
//            rndRotation = Random.Range(-maxRotationAngle, maxRotationAngle);
//
//        GameObject emogy = Instantiate(imagePref, screenSpawn, Quaternion.Euler(0, 0, rndRotation), mainCanvas.transform);
//        emogy.GetComponent<Image>().sprite = spritePull[Random.Range(0, spritePull.Count)];
//
// 
//        FadingDestroyCall(emogy, fadingTime);
    }


    /// <summary>
    /// Spawn both emogy and text via screen bounds, texts are random
    /// </summary>
    public void FullPopupSpawn()
    {
        EmogiSpawn();
        CheeringTextSpawn();
        
    }


    public void FullPopupSpawn(int numberOfImg, int numberOfText)
    {
        for(int i = 0; i < numberOfImg; i++)
        {
            EmogiSpawn();
        }
        for (int i = 0; i < numberOfText; i++)
        {
            CheeringTextSpawn();
        }
        

    }


    public void FadingDestroyCall(GameObject target, float time)
    {
        StartCoroutine(FadingUI(target, time));
    }

    public IEnumerator FadingUI(GameObject target, float time)
    {
        CanvasGroup fadingCom = null;
        if (target == null)
        {
            Debug.LogError("(Fading Destroy)Destroy call without target");
            yield break;
        }
        if (target.GetComponent<CanvasGroup>() == null)
        {
            Debug.Log("(Fading Destroy)Target don't have needed components");
            yield break;
        }
        else
        {
            fadingCom = target.GetComponent<CanvasGroup>();
        }



        float startAlpha = fadingCom.alpha;
        float rate = 1.0f / time;
        float progress = 0.0f;
        //Debug.Log("Destroy start" + progress);
        while (progress < 1.0)
        {
            if (target == null)
            {
                yield break;
            }
            float tmpAplha = fadingCom.alpha;
            fadingCom.alpha = Mathf.Lerp(startAlpha, 0, progress);
            if (target.GetComponent<CanvasGroup>() == null)
                target.GetComponentInChildren<CanvasGroup>().alpha = fadingCom.alpha;
            else
                target.GetComponent<CanvasGroup>().alpha = fadingCom.alpha;

            progress += rate * Time.deltaTime;
            //Debug.Log("Destroy Progress" + progress);
            yield return null;
        }
        Destroy(target);
    }

}
