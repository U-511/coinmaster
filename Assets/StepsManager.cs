﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StepsManager : MonoBehaviour
{
    public static StepsManager singleton;
    public Slider stageSlider;

    private void Awake()
    {
        singleton = this;
    }

    public void AddProgress()
    {
        //stageSlider.value++;
        StartCoroutine(AddSliderProgress());
    }

    public IEnumerator AddSliderProgress()
    {
        int startValue = Mathf.RoundToInt(stageSlider.value);
        int goal = startValue + 1;

        while (stageSlider.value < goal)
        {
            stageSlider.value += Time.deltaTime;
            yield return null;
        }
    }
}
