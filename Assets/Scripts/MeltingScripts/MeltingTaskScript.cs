﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MeltingTaskScript : MonoBehaviour
{
    public static MeltingTaskScript sigleton = null;

    public GameObject cameraTarget;
    public Vector3 chanPos;
    public BucketHolderScript chan;
    public float startOffset;

    [Space]
    public float startTime;
    public float currentTime;

    [Header("Mech progress")]
    public List<TimeZones> goalZones; //Contain all Green zones data(Count == Mech Goal)
    public int currentProgress; //+1 if tapped in right zone, after current progress == Count goalZones, transfer to new mech

    public Vector2 neededTime = new Vector2(5f, 8f); //Min-Max needed time
    public float maxTime = 10f; //Time where will be GameOver
    [Space]
    public Slider progressSlider;
    //public Image fillImage;
    public Image handle;
    [Space]
    //public Gradient progressGrad;
    public Color32 idealZoneColor;
    public Sprite idealZoneSprite;
    public Sprite commonZoneSprite;
    [Space]
    public Text result;
    public Button stopMelt;

    public MeshRenderer[] lights;
    public Material enabledLightMaterial;

    public bool mechInited = false;
    private void Awake()
    {
        sigleton = this;
        progressSlider.gameObject.SetActive(false);
        result.gameObject.SetActive(false);
        stopMelt.gameObject.SetActive(false);
    }

    public void MechInitCall()
    {
        StartCoroutine(MechInit());
       
    }

    public IEnumerator MechInit()
    {
        chan.SetupStart(true);
        
//        iTween.MoveTo(chan.gameObject, iTween.Hash("position", cameraTarget.transform.position, "time", 1.4f, "easetype", iTween.EaseType.easeInOutCirc));
        //iTween.MoveTo(Camera.main.gameObject, iTween.Hash("position", cameraPos, "time", 0.7f, "easetype", iTween.EaseType.easeInOutCirc));

//        Camera.main.gameObject.GetComponent<CameraController>().target = cameraTarget.transform;

        //Wait timer
        yield return new WaitForSecondsRealtime(startOffset);

        startTime = Time.time;
        currentTime = Time.time;
        progressSlider.gameObject.SetActive(true);
        //stopMelt.gameObject.SetActive(true);
        progressSlider.minValue = 0;
        progressSlider.maxValue = maxTime;
        progressSlider.value = currentTime - startTime;
        //fillImage.color = progressGrad.Evaluate(progressSlider.normalizedValue);
        
        //Setup mulitple zones
        for (int i = 0; i < goalZones.Count; i++)
        {
            goalZones[i].greenZone = GetComponent<GreenZoneController>().SetupGreenZoneHorisontal(new Vector2(goalZones[i].startTime / maxTime, (goalZones[i].startTime + goalZones[i].duration) / maxTime));
        }

        mechInited = true;

    }

    public void Update()
    {
        if (mechInited == false)
        {
            return;
        }
        currentTime = Time.time;
        progressSlider.value = currentTime - startTime;
        //fillImage.color = progressGrad.Evaluate(progressSlider.normalizedValue);
        float progress = currentTime - startTime;
        handleInGreenZone(progress);

        chan.UpdateLiquidUp(progress / goalZones[goalZones.Count-1].startTime);
        //if (progress >= neededTime.x && progress <= neededTime.y)
        //{
        //    handle.sprite = idealZoneSprite;
        //}
        //else
        //{
        //    //handle.color = Color.white;
        //    handle.sprite = commonZoneSprite;
        //}

        //Rework, set to check if tapped in any of zone
        //If true add current goal +1
        if (Input.GetMouseButtonDown(0))
        {
            if (IsTappedInZoneCheck(currentTime - startTime) == true)
            {
                if (lights.Length > currentProgress)
                {
                    lights[currentProgress].material = enabledLightMaterial;
                }
                currentProgress++;
            }

            if (currentProgress == goalZones.Count)
            {
                Debug.LogError("Success");
                StopMelting();
            }
        }

        //Player out of time transfer to another mech
        if (currentTime - startTime > maxTime)
        {
            Debug.LogError("Over time");
            StopMelting();
        }
    }

    public void StopMelting()
    {
        mechInited = false;
        stopMelt.gameObject.SetActive(false);
        result.gameObject.SetActive(false);
        //float progress = currentTime - startTime;
        //if (progress >= neededTime.x && progress <= neededTime.y)
        //{
        //    result.text = "Excelent";
        //}
        //else if(progress < neededTime.x)
        //{
        //    result.text = "Too cold";
        //}
        //else if (progress > neededTime.y)
        //{
        //    result.text = "Coin Burned";
        //}

        //Go to next Mech

        StartCoroutine(GoToNextStage());
    }

    public void handleInGreenZone(float currentTime)
    {
        for (int i = 0; i < goalZones.Count; i++)
        {
            if (currentTime >= goalZones[i].startTime && currentTime <= (goalZones[i].startTime + goalZones[i].duration) && goalZones[i].isArchived == false)
            {
                handle.color = idealZoneColor;
                break;
            }
            else
            {
                handle.color = Color.white;
                
            }
        }
    }

    public bool IsTappedInZoneCheck(float currentTime)
    {
        bool tappedInZone = false;

        for (int i = 0; i < goalZones.Count; i++)
        {
            if (currentTime >= goalZones[i].startTime && currentTime <= (goalZones[i].startTime + goalZones[i].duration) && goalZones[i].isArchived == false)
            {
                //Check if current time is in zone
                //If true disable this zone and return true
                goalZones[i].isArchived = true;
                Destroy(goalZones[i].greenZone);

                tappedInZone = true;
                break;
            }
        }

        return tappedInZone;
    }

    IEnumerator GoToNextStage()
    {
        progressSlider.gameObject.SetActive(false);
        Destroy(result.gameObject, 1.4f);

        EmogiNtextPopup.singleton.FullPopupSpawn(3, 1);

        StepsManager.singleton.AddProgress();
        yield return new WaitForSecondsRealtime(3f);
        PouringTaskManager.singleton.MechInit();
    }

   
}

[System.Serializable]
public class TimeZones
{
    public float startTime;
    public float duration;

    public bool isArchived = false;
    public GameObject greenZone = null;
}
