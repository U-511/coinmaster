﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnaceScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Material")
        {
            ForgeTaskScript.singleton.AddMaterial(other.gameObject);
            //Destroy(other.gameObject, 0.5f);
        }
    }
}
