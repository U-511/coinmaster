﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanLiquidScript : MonoBehaviour
{
    public GameObject Liquid;

    public GameObject LiquidMesh;

    public float _mSloshSpeed = 600;

    public float _mRotateSpeed = 15;

    public int _difference = 25;

    
    void Update()
    {
        Slosh();
        
        //LiquidMesh.transform.Rotate(Vector3.back * _mRotateSpeed * Time.deltaTime, Space.Self);
    }

    /// <summary>
    /// Update rotation of liquid, by invert local rotation of bucket
    /// </summary>
    private void Slosh()
    {
        var inverseRotation = Quaternion.Inverse(transform.localRotation);
        var finalRotation = Quaternion
            .RotateTowards(Liquid.transform.localRotation, inverseRotation, _mSloshSpeed * Time.deltaTime).eulerAngles;

        finalRotation.x = ClampRotationValue(finalRotation.x, _difference);
        finalRotation.z = ClampRotationValue(finalRotation.z, _difference);

        Liquid.transform.localEulerAngles = finalRotation;
    }

    private float ClampRotationValue(float value, float difference)
    {
        var returnValue = 0.0f;
        if (value > 180)
        {
            returnValue = Mathf.Clamp(value, 360 - difference, 360);
        }
        else
        {
            returnValue = Mathf.Clamp(value, 0, difference);
        }

        return returnValue;
    }
}
