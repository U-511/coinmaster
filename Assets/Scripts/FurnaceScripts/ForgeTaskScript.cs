﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ForgeTaskScript : MonoBehaviour
{
    public Vector3 cameraPos;
    public Vector3 cameraRotation;

    public GameObject chan;

    public static ForgeTaskScript singleton = null;

    public int materialGoal;
    public int currentMaterial;

    public Text goalTxt;
    public GameObject nextStepButton;

    public GameObject Tube;
    public GameObject Konforka;

    public bool initMech = false;

    private void Awake()
    {
        nextStepButton.SetActive(false);
        if (singleton == null)
        {
            singleton = this;
        }
        else
        {
            Debug.LogError("Another Forge Task Script on scene");
        }

    }

    public void Start()
    {
        GetComponent<MaterialSpawner>().StartSpawner();
        Camera.main.transform.position = cameraPos;
        Vector3 cameraRot = Camera.main.transform.eulerAngles;
        cameraRot.z = cameraRotation.z;
        Camera.main.transform.rotation = Quaternion.Euler(cameraRot);

        goalTxt.gameObject.SetActive(true);
        initMech = true;
    }

    public void StopMech()
    {
        GetComponent<MaterialSpawner>().StopAllCoroutines();
        initMech = false;
    }

    private void OnGUI()
    {
        goalTxt.text = "Goal " + currentMaterial + "/" + materialGoal; 
    }

    public void AddMaterial(GameObject _target)
    {
        if (initMech == false)
            return;

        currentMaterial++;
        _target.transform.parent = chan.transform;
        if (currentMaterial >= materialGoal)
        {
            //TurnOnNextStepButton();
            GoToMeltingStep();
        }
    }

    public void TurnOnNextStepButton()
    {
        nextStepButton.SetActive(true);
    }

    public void TurnOffNextStepButton()
    {
        nextStepButton.SetActive(false);
    }

    public IEnumerator MeltingTransition()
    {
        StopMech();
        TurnOffNextStepButton();
        goalTxt.gameObject.SetActive(false);

        EmogiNtextPopup.singleton.FullPopupSpawn();
        var goldItems = GetComponent<MaterialSpawner>().SpawnedItems;

        StepsManager.singleton.AddProgress();

        yield return new WaitForSecondsRealtime(3f);
        iTween.MoveTo(Tube, iTween.Hash("position", Tube.transform.position + new Vector3(0,5,0), "time", 1.0f, "easetype", iTween.EaseType.easeOutCubic));
        iTween.RotateTo(Konforka, iTween.Hash("rotation", new Vector3(0,0,0), "time", 1.0f, "easetype", iTween.EaseType.easeInCubic));
        yield return new WaitForSecondsRealtime(1f);
        
        foreach (var goldItem in goldItems)
        {
            if (goldItem != null)
            {
                goldItem.transform.parent = chan.transform;
            }
        }

        chan.GetComponent<BucketHolderScript>().SpawnedItems = goldItems.ToList();
       
        MeltingTaskScript.sigleton.MechInitCall();
    }

    public void GoToMeltingStep()
    {
        StartCoroutine(MeltingTransition());
    }

}
