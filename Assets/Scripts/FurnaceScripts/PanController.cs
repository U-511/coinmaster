﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanController : MonoBehaviour
{
    public GameObject pan;
    public Vector3 basePanRot;


    public Vector3 prePos =Vector3.zero;
    public Vector3 deltaPos = Vector3.zero;

    public float sensivetyDecrease = 25;

    private void Update()
    {
        deltaPos = Input.mousePosition - prePos;
        if (Input.GetMouseButton(0))
        {
            pan.transform.Rotate(0, 0, -Vector3.Dot(deltaPos / sensivetyDecrease, Camera.main.transform.up));
        }
        else
        {
            pan.transform.rotation = Quaternion.Slerp(pan.transform.rotation, Quaternion.Euler(basePanRot), 0.2f);
        }
        prePos = Input.mousePosition;

        //if (Input.GetMouseButton(0))
        //{
        //    Vector3 _targetPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        //    Debug.Log("Mouse Point on:" + _targetPos);
        //    var angle = Vector2.Angle(Vector2.right, _targetPos - pan.transform.position);
        //    angle -= 90;
        //    pan.transform.eulerAngles = new Vector3(0, 0, pan.transform.position.y < _targetPos.y ? angle : -angle);
        //}

    }
}
