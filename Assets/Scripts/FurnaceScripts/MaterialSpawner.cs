﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialSpawner : MonoBehaviour
{
    public List<GameObject> materialPrefs;

    public int materialsBeforeCD;
    public float cooldownTime;

    public Vector3 rnd;
    public GameObject spawnerPos;
    public Coroutine spawnerCor = null;

    [HideInInspector] 
    public List<GameObject> SpawnedItems;
    
    public void StartSpawner()
    {
        //StartCoroutine(Spawner());
    }


    private void Update()
    {
        if (ForgeTaskScript.singleton.initMech == false)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            if (spawnerCor == null)
            {
                spawnerCor = StartCoroutine(Spawner());
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            spawnerCor = null;
            StopAllCoroutines();
        }
            
    }

    public IEnumerator Spawner()
    {
        while (true)
        {
            //Take rnd material, spawn under pan
            for (int i = 0; i < materialsBeforeCD; i++)
            {
                GameObject mat = materialPrefs[ Random.Range(0, materialPrefs.Count)];
                float rndX = Random.Range(0, rnd.x) * Random.Range(-1, 1);
                float rndY = Random.Range(0, rnd.y) * Random.Range(-1, 1);
                float rndZ = Random.Range(0, rnd.z) * Random.Range(-1, 1);
                GameObject tempMap = Instantiate(mat, spawnerPos.transform.position + new Vector3(rndX, rndY, rndZ), Quaternion.identity);
                SpawnedItems.Add(tempMap);
                
//                Destroy(tempMap, 25f);
                yield return null;
            }
            //Go to Cooldown

            yield return new WaitForSecondsRealtime(cooldownTime);
        }
    }

}
