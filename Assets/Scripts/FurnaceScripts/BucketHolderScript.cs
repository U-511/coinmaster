﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucketHolderScript : MonoBehaviour
{    
    [Space]
    public GameObject liquid;
    public float startPlaneHeight;
    public float endPlaneHeight;
    [Space]
    public float startPlaneSize;
    public float endPlaneSize;
    [Space]
    public float startItemScale;
    public float progressItemScale;
    
    [HideInInspector] public List<GameObject> SpawnedItems;
    // Start is called before the first frame update

    public void UpdateLiquid(float progress)
    {
        float fluidHeight = Mathf.Lerp(startPlaneHeight, endPlaneHeight, progress);
        float fluidSize = Mathf.Lerp(startPlaneSize, endPlaneSize, progress);

        Vector3 pos = liquid.transform.localPosition;
        pos.y = fluidHeight;
        liquid.transform.localPosition = pos;

        Vector3 scale = liquid.transform.localScale;
        scale.x = fluidSize;
        scale.z = fluidSize;
        liquid.transform.localScale = scale;
    }
    
    public void UpdateLiquidUp(float progress)
    {
        if (SpawnedItems != null && SpawnedItems.Count > 0)
        {
            var newSize = startItemScale - progress * progressItemScale;
            var newScale = new Vector3(newSize,newSize,newSize);
            for (var i = 0; i < SpawnedItems.Count - 1; i++)
            {
                if (SpawnedItems[i] != null)
                {

                    SpawnedItems[i].transform.localScale = newScale;
                }
            }
        }

        float fluidHeight = Mathf.Lerp(endPlaneHeight, startPlaneHeight, progress);
        float fluidSize = Mathf.Lerp(endPlaneSize, startPlaneSize, progress);

        Vector3 pos = liquid.transform.localPosition;
        pos.y = fluidHeight;
        liquid.transform.localPosition = pos;

        Vector3 scale = liquid.transform.localScale;
        scale.x = fluidSize;
        scale.z = fluidSize;
        liquid.transform.localScale = scale;
    }

    public void SetupStart(bool empty)
    {
        liquid.SetActive(true);
        if (empty)
        {
            Vector3 pos = liquid.transform.localPosition;
            pos.y = endPlaneHeight;
            liquid.transform.localPosition = pos;

            Vector3 scale = liquid.transform.localScale;
            scale.x = endPlaneSize;
            scale.z = endPlaneSize;
            liquid.transform.localScale = scale;
        }
        else
        {
            SpawnedItems.Clear(); // items will be destroyed by trigger Material
            
            Vector3 pos = liquid.transform.localPosition;
            pos.y = startPlaneHeight;
            liquid.transform.localPosition = pos;

            Vector3 scale = liquid.transform.localScale;
            scale.x = startPlaneSize;
            scale.z = startPlaneSize;
            liquid.transform.localScale = scale;
        }
    }
}
