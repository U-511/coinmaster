﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GreenZoneController : MonoBehaviour
{
    public RectTransform Slider;

    public Vector2 zonePercentage;
    public bool isHorisontal;

    public Rect targetRect;

    public GameObject zonePref; //Transform into prefab
    public GameObject zoneHolder; //Prefab holder

    public Sprite zoneSprite;

    //Setup green zone
    //Take percentage value of height(width) of slider;
    //Set green zone start point at height, calculate height of green zone by slider percent y
    //Set green zone width and height and sprite


    public GameObject SetupGreenZone(Vector2 _zonePercentage)
    {
        zonePercentage = _zonePercentage;
        if (isHorisontal == true)
        {
            Debug.LogError("Try to setup Hor type");
            return null;
        }


        float minHeight = Slider.rect.height * zonePercentage.x;
        float maxHeight = Slider.rect.height * zonePercentage.y;


        targetRect.x = 0;
        targetRect.y = Slider.rect.y + (minHeight + maxHeight)/2;

        targetRect.width = Slider.rect.width;
        targetRect.height = Mathf.Abs(maxHeight - minHeight);

        GameObject greenZoneObj = Instantiate(zonePref, zoneHolder.transform);

        RectTransform zoneRect = greenZoneObj.GetComponent<RectTransform>();
        zoneRect.anchoredPosition = new Vector2(targetRect.x, targetRect.y);
        zoneRect.sizeDelta = new Vector2(targetRect.width, targetRect.height);
        greenZoneObj.GetComponent<Image>().sprite = zoneSprite;

        return greenZoneObj;
    }

    public GameObject SetupGreenZoneHorisontal(Vector2 _zonePercentage)
    {
        zonePercentage = _zonePercentage;

        float minWidth = Slider.rect.width * zonePercentage.x;
        float maxWidth = Slider.rect.width * zonePercentage.y;

        targetRect.x = Slider.rect.x + (minWidth + maxWidth) / 2;
        targetRect.y = 0;

        targetRect.width = Mathf.Abs(maxWidth - minWidth);
        targetRect.height = Slider.rect.height;

        GameObject greenZoneObj = Instantiate(zonePref, zoneHolder.transform);

        RectTransform zoneRect = greenZoneObj.GetComponent<RectTransform>();
        zoneRect.anchoredPosition = new Vector2(targetRect.x, targetRect.y);
        zoneRect.sizeDelta = new Vector2(targetRect.width, targetRect.height);
        greenZoneObj.GetComponent<Image>().sprite = zoneSprite;

        return greenZoneObj;
    }
}
