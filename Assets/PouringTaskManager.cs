﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PouringTaskManager : MonoBehaviour
{
    public static PouringTaskManager singleton;
    
    [Space]
    public GameObject Konforka;
    public GameObject Plita;
    
    [Space]
    public GameObject cameraTarget;
    public Vector3 chanPos;
    [Header("Chan settings")]
    public BucketHolderScript chan;
    public ParticleSystem chanPouringEffect;
    public int emitForce;
    public Vector2 chanGoalRotation;
    public Vector2 chanMaxRotation;
    [Space]
    public GameObject liquid;
    public float startPlaneHeight;
    public float endPlaneHeight;
    [Space]
    public float startPlaneSize;
    public float endPlaneSize;

    [Header("Mech settings")]
    public float pouringGoal = 100;
    public float progress = 0;

    [Space]
    public bool mechInit = false;
    public bool goalRiched = false;

    [Header("Mech Targets settings")]
    public GameObject fluidCoin;
    public GameObject solidCoin;

    public Vector3 startCoinPos;
    public Vector3 endCoinPos;
    
    [Space]
    public Text resultText;
    [Space]
    public Vector3 startPos;
    public Vector3 endPos;

    public Vector3 deltaPos = Vector3.zero;
    public Vector3 prePos = Vector3.zero;

    private Coroutine animCour;

    public float sensivetyDecrease = 25;

    [Header("Variables for Model")]
    public Transform[] listOfVeticiesObjects;
    public Vector3[] listOfVeticies = new Vector3[6];
    public GameObject meshGameObject;
    public Material meshMat;

    private void Awake()
    {
        singleton = this;
    }

    public void MechInit()
    {
        chan.SpawnedItems.ForEach(Destroy);
        //Start Swipe check
        
        iTween.MoveTo(Plita, iTween.Hash("position", Konforka.transform.position + new Vector3(0.0f, 1.0f, 0), "time", 1.0f, "easetype", iTween.EaseType.easeInOutCirc));
        startCoinPos = Konforka.transform.position + new Vector3(0.0f, 0.65f, 0);
        endCoinPos = startCoinPos + new Vector3(0.0f, 0.3f, 0);
        iTween.MoveTo(fluidCoin, iTween.Hash("position", startCoinPos, "time",1.05f, "easetype", iTween.EaseType.easeInOutCirc));
        iTween.MoveTo(solidCoin, iTween.Hash("position", Konforka.transform.position + new Vector3(0.0f, 0.5f, 0), "time",1.0f, "easetype", iTween.EaseType.easeInOutCirc));
        iTween.MoveTo(Konforka, iTween.Hash("position", Konforka.transform.position + new Vector3(-15, 0, 0), "time", 1.0f, "easetype", iTween.EaseType.easeInOutCirc));
        iTween.MoveTo(chan.gameObject, iTween.Hash("position", chan.gameObject.transform.position + new Vector3(0.9f, 1.1f, 0), "time", 1.4f, "easetype", iTween.EaseType.easeInOutCirc));
        //iTween.MoveTo(Camera.main.gameObject, iTween.Hash("position", cameraPos, "time", 0.7f, "easetype", iTween.EaseType.easeInOutCirc));
//        Camera.main.gameObject.GetComponent<CameraController>().target = cameraTarget.transform;
        mechInit = true;
        
    }

    //Check for swipe, if swipe was down start rotation and timer
    private void FixedUpdate()
    {
        if (mechInit == false)
            return;

        deltaPos = Input.mousePosition - prePos;
        if (Input.GetMouseButton(0) && goalRiched == false)
        {
            if (chan.transform.eulerAngles.z < chanMaxRotation.y)
                chan.transform.Rotate(0, 0, -Vector3.Dot(deltaPos / sensivetyDecrease, Camera.main.transform.up));
            else
            {
                chan.transform.rotation = Quaternion.Slerp(chan.transform.rotation, Quaternion.Euler(Vector3.zero), 0.08f);
            }
            
        }
        else
        {
            chan.transform.rotation = Quaternion.Slerp(chan.transform.rotation, Quaternion.Euler(Vector3.zero), 0.03f);
        }
        prePos = Input.mousePosition;


        if (chan.transform.localRotation.eulerAngles.z > chanGoalRotation.x
            && chan.transform.localRotation.eulerAngles.z < chanGoalRotation.y)
        {
            resultText.gameObject.SetActive(true);
            resultText.text = "Keep it";
            chanPouringEffect.Play();
            if (animCour == null && goalRiched == false)
                animCour = StartCoroutine(PouringAnim());
        }
        else if (chan.transform.localRotation.eulerAngles.z <= chanGoalRotation.x || chan.transform.localRotation.eulerAngles.z > 180)
        {
            if (goalRiched == false)
                StopAllCoroutines();
            Destroy(meshGameObject);
            animCour = null;
            resultText.text = "";
            chanPouringEffect.Stop();
        }
        else if (chan.transform.localRotation.eulerAngles.z >= chanGoalRotation.y)
        {
            chanPouringEffect.Play();
            if (goalRiched == false)
                StopAllCoroutines();
            Destroy(meshGameObject);
            animCour = null;
            resultText.text = "";
        }
    }
    
    public IEnumerator PouringAnim()
    { 
        
        //yield return new WaitForSecondsRealtime(0.15f);
        progress += 1;
        fluidCoin.transform.position = Vector3.Lerp(startCoinPos, endCoinPos, progress / pouringGoal);

        chan.UpdateLiquid(progress / pouringGoal);

        if (progress >= pouringGoal)
        {
            goalRiched = true;
            //resultText.gameObject.SetActive(true);
            //resultText.text = "AWESOME";
            resultText.gameObject.SetActive(false);
            
            chanPouringEffect.Stop();
            EmogiNtextPopup.singleton.FullPopupSpawn();
            StepsManager.singleton.AddProgress();

            yield return new WaitForSecondsRealtime(2f);
            mechInit = false;
            //Destroy fluid coin, set solid coin
            fluidCoin.SetActive(false);
            solidCoin.transform.position = fluidCoin.transform.position;
            solidCoin.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);

            yield return new WaitForSecondsRealtime(0.8f);
            solidCoin.transform.localScale = new Vector3(1f, 1f, 1f);
            
            ForgeTaskManager.singleton.MechInit();
        }
        
    }

    public void CreateMesh()
    {
        RaycastHit hit;
        if (Physics.Raycast(listOfVeticiesObjects[1].position, Vector3.down, out hit))
        {
            listOfVeticiesObjects[4].position = hit.point;
            listOfVeticies[1] = listOfVeticiesObjects[1].position;
            listOfVeticies[4] = listOfVeticiesObjects[4].position;
        }

        if (Physics.Raycast(listOfVeticiesObjects[2].position, Vector3.down, out hit))
        {
            listOfVeticiesObjects[5].position = hit.point;
            listOfVeticies[2] = listOfVeticiesObjects[2].position;
            listOfVeticies[5] = listOfVeticiesObjects[5].position;
        }

        if (Physics.Raycast(listOfVeticiesObjects[0].position, Vector3.down, out hit))
        {
            listOfVeticiesObjects[3].position = hit.point;
            listOfVeticies[0] = listOfVeticiesObjects[0].position;
            listOfVeticies[3] = listOfVeticiesObjects[3].position;
        }


        int[] triangles = new int[21];
        //I
        triangles[0] = 0;
        triangles[1] = 2;
        triangles[2] = 3;

        triangles[3] = 3;
        triangles[4] = 2;
        triangles[5] = 5;
        //II
        triangles[6] = 1;
        triangles[7] = 0;
        triangles[8] = 4;

        triangles[9] = 4;
        triangles[10] = 0;
        triangles[11] = 3;

        //III
        triangles[12] = 2; 
        triangles[13] = 1;
        triangles[14] = 5;

        triangles[15] = 5;
        triangles[16] = 2;
        triangles[17] = 4;

        //IV
        triangles[18] = 0;
        triangles[19] = 1;
        triangles[20] = 2;



        Mesh mesh = new Mesh();
        mesh.vertices = listOfVeticies;
        
        mesh.triangles = triangles;
       

        if (meshGameObject == null)
        {
            meshGameObject = new GameObject("Mesh", typeof(MeshFilter), typeof(MeshRenderer));
        }
        meshGameObject.GetComponent<MeshFilter>().mesh = mesh;
        meshGameObject.GetComponent<MeshRenderer>().material = meshMat;
    }

}
