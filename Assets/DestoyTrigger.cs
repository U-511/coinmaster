﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoyTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Material")
        {
            Destroy(other.gameObject);
        }
    }
}
