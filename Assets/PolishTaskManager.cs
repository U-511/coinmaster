﻿using System.Collections;
using System.Collections.Generic;
using Es.InkPainter.Sample;
using UnityEngine;
using UnityEngine.UI;

public class PolishTaskManager : MonoBehaviour
{
    public static PolishTaskManager singleton;
    public GameObject coin;
    public GameObject hummer;

    [Header("Camera setup")]
    public GameObject cameraTarget;
    public float cameraDistMulty;
    public float cameraHieghtMulty;
    [Header("Task goal")]
    public int faceGoal;
    public int rearGoal;
    [Space]
    public float faceProgress;
    public float rearProgress;

    [Header("Progress VFX")]
    public Slider facePrg;
    public Slider rearPrg;

    [Header("Towel Data")]
    public Image Towel;
    public Vector3 rotateDir;
    public Vector3 offset;
    public float deltaRotate;

    [Header("Mech var")]
    public bool coinSide = false; //false == face, true == rear
    public float mouseMotion;
    private Vector3 currentMousePos;
    private Vector3 lastMousePos;

    private Vector3 startPos;
    public bool isInit = false;
    

    private void Awake()
    {
        singleton = this;
    }

    public void MechInit()
    {
        var newCoinPos = coin.transform.position + new Vector3(0, 5, 0);
        iTween.MoveTo(hummer, iTween.Hash("position", hummer.transform.position + new Vector3(-5f, 0f, 0), "time", 1.0f, "easetype", iTween.EaseType.easeInOutCirc));
        iTween.MoveTo(coin, iTween.Hash("position", newCoinPos, "time", 0.5f, "easetype", iTween.EaseType.easeInOutCirc));
        iTween.RotateTo(coin, iTween.Hash("rotation", new Vector3(270,0,0), "time", 0.5f, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(Camera.main.gameObject, iTween.Hash("position", cameraTarget.transform, "time", 0.5f, "easetype", iTween.EaseType.easeInOutCirc));
        iTween.RotateTo(Camera.main.gameObject, iTween.Hash("rotation", new Vector3(0,0,0), "time", 0.5f, "easetype", iTween.EaseType.easeInOutCirc));
//        Camera.main.gameObject.GetComponent<CameraController>().target = cameraTarget.transform;
        Camera.main.gameObject.GetComponent<MousePainter>().enabled = true;
       
//        facePrg.gameObject.SetActive(true);
//        rearPrg.gameObject.SetActive(true);

        facePrg.maxValue = faceGoal;
        rearPrg.maxValue = rearGoal;

        isInit = true;
    }


    private void Update()
    {
        if (isInit == false)
            return;
        Camera.main.gameObject.GetComponent<CameraController>().cameraDistanceMultiplier = cameraDistMulty;
        Camera.main.gameObject.GetComponent<CameraController>().cameraYMultiplier = cameraHieghtMulty;
        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
            currentMousePos = Input.mousePosition;
            lastMousePos = currentMousePos;
            Towel.gameObject.SetActive(true);
        }

        if (Input.GetMouseButton(0))
        {
            currentMousePos = Input.mousePosition;
            mouseMotion = Vector3.Distance(currentMousePos, lastMousePos);

            rotateDir = (currentMousePos - lastMousePos);
            //deltaRotate = Mathf.DeltaAngle(Mathf.Atan2(lastMousePos.y, lastMousePos.x) * Mathf.Rad2Deg,
            //                    Mathf.Atan2(currentMousePos.y, currentMousePos.x) * Mathf.Rad2Deg);
            deltaRotate = Vector3.Angle(Vector2.right, rotateDir);

            
            Towel.transform.position = currentMousePos+ offset;

            //Towel.transform.eulerAngles = new Vector3(0f, 0f, lastMousePos.y < currentMousePos.y ? -deltaRotate : deltaRotate);
            if (coinSide == false)
            {
                faceProgress += mouseMotion / 10;
                facePrg.value = faceProgress;
            }
            else
            {
                rearProgress += mouseMotion / 10;
                rearPrg.value = rearProgress;
            }
            ProgressCheck();
            lastMousePos = currentMousePos;
        }

        if (Input.GetMouseButtonUp(0))
        {
            //Check swipe left - right
            currentMousePos = Input.mousePosition;
            

            if (Mathf.Abs(currentMousePos.x - startPos.x) > Screen.width * 0.8)
            {
                coinSide = !coinSide;
                //Debug.LogError("Swipe");
                if((currentMousePos.x - startPos.x) != 0)
                {
                    //Debug.LogError("SwipeLeft-Right");
                    if (coinSide == true)
                    {
                        iTween.RotateTo(coin, new Vector3(-90, 0, 180), 0.2f);
                    }
                    else
                    {
                        iTween.RotateTo(coin, new Vector3(-90, 0, 0), 0.2f);
                    }
                   
                }

                //if ((currentMousePos.x - startPos.x) < 0)
                //{
                //    //Debug.LogError("SwipeRight-Left");
                //    iTween.RotateTo(coin, new Vector3(-90, 0, coin.transform.localEulerAngles.z - 180), 0.2f);
                //    return;
                //}

                

            }
            Towel.gameObject.SetActive(false);
            currentMousePos = Vector3.zero;
            lastMousePos = Vector3.zero;

        }
    }

    public void ProgressCheck()
    {
        if (rearProgress >= rearGoal && faceProgress >= faceGoal && isInit == true)
        {
            EmogiNtextPopup.singleton.FullPopupSpawn();
            StepsManager.singleton.AddProgress();
            Towel.gameObject.SetActive(false);
            isInit = false;
        }
    }
}
