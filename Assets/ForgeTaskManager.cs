﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForgeTaskManager : MonoBehaviour
{
    public static ForgeTaskManager singleton;

    [Header("Influenced GameObjects")]
    public GameObject hammer;
    public ParticleSystem hammerHitEffect;
    public GameObject coin;
    public Vector3 coinPosition;
    public GameObject anvil;
    public BucketHolderScript chan;
    public GameObject plita;

    [Header("Hammer anim")]
    public GameObject hummer;
    public float currHammerAngle;
    public float maxHammerAngle;
    public float hammerTime;

    [Header("Taps and hit force settings")]
    public float currentTapTime;
    public float maxTapTime; //CurrentTap Time / max tap time == percent of hit
    public Vector2 hitForcePercent; //3 zonez 0-X light hit X-Y medium hit Y - 100 heavy hit

    [Header("Task goal and progress")]
    public int goal;
    public int progress;
    [Space]
    public ZoneSpawnType spawnType;
    [Space]
    public Vector2 rndZoneDuration;
    public List<TimeZones> goalZones; //Contain all Green zones data(Count == Mech Goal)

    [Header("Camera and GameObject settings")]
    public Transform cameraTarget;
    

    [Header("UI connection")]
    public Slider tapForceSlider;
    public Image sliderImage;
    public Gradient forceGrad;
    public Text progressTxt;

    [Header("Mech variables")]
    public bool mechInit = false;
    public Coroutine bashEffect = null;

    private void Awake()
    {
        singleton = this;
    }

    public void MechInit()
    {
        var hummerOffset = hummer.transform.position - anvil.transform.position;
        var anvilTargetPosition = plita.transform.position + new Vector3(0f, 0f, 0);
        //Transport coin to anvil
        iTween.MoveTo(chan.gameObject, iTween.Hash("position", chan.gameObject.transform.position + new Vector3(0f, 10f, 0), "time", 1.4f, "easetype", iTween.EaseType.easeInOutCirc));
        iTween.MoveTo(anvil, iTween.Hash("position", anvilTargetPosition, "time", 1.4f, "easetype", iTween.EaseType.easeInOutCirc));
        iTween.MoveTo(hummer, iTween.Hash("position", anvilTargetPosition + new Vector3(0,0,-10), "time", 1.0f, "easetype", iTween.EaseType.easeInOutCirc));
        iTween.MoveTo(hummer, iTween.Hash("position", anvilTargetPosition + hummerOffset, "time", 1.0f, "easetype", iTween.EaseType.easeInOutCirc, "delay", 3.0f));
        iTween.MoveTo(plita, iTween.Hash("position", plita.transform.position + new Vector3(-5f, 0f, 0), "time", 1.4f, "easetype", iTween.EaseType.easeInOutCirc));
        
        iTween.MoveTo(coin, iTween.Hash("position", coin.transform.position + new Vector3(0f, 4f, 0), "time", 2.0f, "easetype", iTween.EaseType.easeOutExpo));
        iTween.MoveTo(coin, iTween.Hash("position", coin.transform.position + new Vector3(0f, 1f, 0), "time", 0.5f, "easetype", iTween.EaseType.easeInOutCirc, "delay", 2.0f));
        
        iTween.RotateBy(coin, iTween.Hash("x", 10.0f , "time", 2.5f));
//        iTween.RotateTo(coin, iTween.Hash("rotation", new Vector3(180,0,0), "time", 0.5f));
//        iTween.RotateTo(coin, iTween.Hash("rotation", new Vector3(360,0,0), "time", 0.5f, "delay", 0.5f, "easetype", iTween.EaseType.linear));
//        iTween.RotateTo(coin, iTween.Hash("rotation", new Vector3(180,0,0), "time", 0.5f, "delay", 1.0f, "easetype", iTween.EaseType.linear));
//        iTween.RotateTo(coin, iTween.Hash("rotation", new Vector3(360,0,0), "time", 0.5f, "delay", 1.5f, "easetype", iTween.EaseType.linear));
//        iTween.RotateTo(coin, iTween.Hash("rotation", new Vector3(180,0,0), "time", 0.5f, "delay", 2.0f, "easetype", iTween.EaseType.linear));
//        iTween.RotateTo(coin, iTween.Hash("rotation", new Vector3(360,0,0), "time", 0.5f, "delay", 2.5f, "easetype", iTween.EaseType.linear));
        //iTween.MoveTo(Camera.main.gameObject, iTween.Hash("position", cameraPos, "time", 1f, "easetype", iTween.EaseType.easeInOutCirc));
//        Camera.main.gameObject.GetComponent<CameraController>().target = cameraTarget.transform;
        mechInit = true;
        //Transport and rotate?? camera
        //Activate tap waiter for Update

        tapForceSlider.gameObject.SetActive(true);
        progressTxt.gameObject.SetActive(true);
        
        progressTxt.text = "Progress: " + progress + "/" + goal;

        switch (spawnType)
        {
            case ZoneSpawnType.ZoneList:
                for (int i = 0; i < goalZones.Count; i++)
                {
                    goalZones[i].greenZone = GetComponent<GreenZoneController>().SetupGreenZoneHorisontal(new Vector2(goalZones[i].startTime / maxTapTime,
                        (goalZones[i].startTime + goalZones[i].duration) / maxTapTime));
                }
                break;

            case ZoneSpawnType.ZoneRndList:
                for (int i = 0; i < goalZones.Count; i++)
                {
                    TimeZones rndTemp = goalZones[i];
                    float rndDurat = Random.Range(rndZoneDuration.x, rndZoneDuration.y);

                    rndTemp.startTime = Random.Range(0, maxTapTime - rndDurat);
                    rndTemp.duration = rndDurat;

                    goalZones[i] = rndTemp;

                    goalZones[i].greenZone = GetComponent<GreenZoneController>().SetupGreenZoneHorisontal(new Vector2(goalZones[i].startTime / maxTapTime,
                        (goalZones[i].startTime + goalZones[i].duration) / maxTapTime));
                }
                break;

            case ZoneSpawnType.ZoneRndSingle:
                goalZones.Clear();
                break;
        }

        
    }

    private void Update()
    {
        if (mechInit == false)
            return;

        if (Input.GetMouseButton(0))
        {
            currentTapTime += Time.deltaTime;
            //Debug.Log($"<color=blue>{currentTapTime}</color>", gameObject);
            //Update slider
            tapForceSlider.value = currentTapTime / maxTapTime;
            //sliderImage.color = forceGrad.Evaluate(tapForceSlider.normalizedValue);

            

            currHammerAngle = currentTapTime / maxTapTime * maxHammerAngle;

            hammer.gameObject.transform.localEulerAngles = new Vector3(currHammerAngle, 0, 0);

            //if zone type == rnd single zone, and list more then one, overwrite zone data
            if (spawnType == ZoneSpawnType.ZoneRndSingle && goalZones.Count != 1)
            {
                AddRndZone();
            }

            //Add check for handle colorise
            if (spawnType == ZoneSpawnType.ZoneRndSingle)
            {
                handleInGreenZone(currentTapTime, goalZones[0]);
            }
            else
            {
                handleInGreenZone(currentTapTime);
            }

            //Change, to slider go backwards
            if (currentTapTime / maxTapTime > 1)
            {
                AddProgress(currentTapTime / maxTapTime);
                iTween.RotateTo(hammer, iTween.Hash("rotation", Vector3.zero, "time", hammerTime, "easetype", iTween.EaseType.easeInOutCirc));
                if (bashEffect == null)
                    bashEffect = StartCoroutine(HammerBashEffect(currentTapTime / maxTapTime));

                tapForceSlider.value = 0;
                currentTapTime = 0;
                //hammer.transform.rotation = Quaternion.Slerp(hammer.transform.rotation, Quaternion.Euler(Vector3.zero), 0.2f);
                
                currHammerAngle = 0;
                return;
            }
        }
        //Player stop holding finger
        else if (Input.GetMouseButtonUp(0))
        {
            if (spawnType == ZoneSpawnType.ZoneRndSingle)
            {
                TimeZones tempZone = new TimeZones();
                tempZone.startTime = goalZones[0].startTime;
                tempZone.duration = goalZones[0].duration;
                tempZone.greenZone = goalZones[0].greenZone;
                AddProgress(currentTapTime, tempZone);
            }
            else
            {
                AddProgress(currentTapTime);
            }
                

            iTween.RotateTo(hammer, iTween.Hash("rotation", Vector3.zero, "time", hammerTime, "easetype", iTween.EaseType.easeInOutCirc));
            if (bashEffect == null)
                bashEffect = StartCoroutine(HammerBashEffect(currentTapTime / maxTapTime));

            tapForceSlider.value = 0;
            currentTapTime = 0;
            //hammer.transform.rotation = Quaternion.Slerp(hammer.transform.rotation, Quaternion.Euler(Vector3.zero), 0.2f);
            
            currHammerAngle = 0;

            //If zone type == rnd single, clear list
            if (spawnType == ZoneSpawnType.ZoneRndSingle)
            {
                goalZones.Clear();
            }
        }

    }
    public void AddRndZone()
    {
        goalZones.Clear();

        TimeZones tempZone = new TimeZones();
        float rndDur = Random.Range(rndZoneDuration.x, rndZoneDuration.y);
        float rndStart = Random.Range(0, maxTapTime - rndDur);

        tempZone.startTime = rndStart;
        tempZone.duration = rndDur;

        tempZone.greenZone = GetComponent<GreenZoneController>().SetupGreenZoneHorisontal(new Vector2(rndStart / maxTapTime, (rndStart + rndDur) / maxTapTime));

        goalZones.Add(tempZone);
    }

    public void AddProgress(float _currTime, TimeZones rndZone = null)
    {
        //Debug.Log($"<color=#FF1111>{_force}</color>", gameObject);
        //if (_force >= 0 && _force < hitForcePercent.x)
        //{
        //    Debug.Log($"<color=#FF6E00>Light Hit</color>", gameObject);
        //    if(progress > 0)
        //        progress -= 1;
        //}

        //if (_force > hitForcePercent.x && _force <= hitForcePercent.y)
        //{
        //    Debug.Log($"<color=#98FF68>Medium Hit</color>", gameObject);
        //    progress += 1;
        //    if (progress > goal)
        //    {
        //        progress += -2;
        //    }
        //}

        //if (_force > hitForcePercent.y)
        //{
        //    Debug.Log($"<color=#611E29>Heavy Hit</color>", gameObject);
        //    progress += 2;
        //    if (progress > goal)
        //    {
        //        progress += -4;
        //    }
        //}

        //Check for zones, based on spawn type
        if (IsTappedInZoneCheck(_currTime, rndZone))
        {
            progress += 1;
        }

        progressTxt.text = "Progress: " + progress + "/" + goal;
        
    }

    public bool IsTappedInZoneCheck(float currentTime, TimeZones _zone = null)
    {
        bool tappedInZone = false;

        switch (spawnType)
        {
            case ZoneSpawnType.ZoneList:
            case ZoneSpawnType.ZoneRndList:
                for (int i = 0; i < goalZones.Count; i++)
                {
                    if (currentTime >= goalZones[i].startTime && currentTime <= (goalZones[i].startTime + goalZones[i].duration) && goalZones[i].isArchived == false)
                    {
                        //Check if current time is in zone
                        //If true disable this zone and return true
                        goalZones[i].isArchived = true;
                        Destroy(goalZones[i].greenZone);
                        tappedInZone = true;
                        return tappedInZone;
                    }
                }
                break;
            case ZoneSpawnType.ZoneRndSingle:
                if (_zone == null)
                {
                    Debug.LogError("Zone not spawned");
                    return tappedInZone;
                }
                if (currentTime >= _zone.startTime && currentTime <= (_zone.startTime + _zone.duration))
                {
                    //Check if current time is in zone
                    //If true disable this zone and return true
                    _zone.isArchived = true;
                    Destroy(_zone.greenZone);

                    tappedInZone = true;
                    return tappedInZone;
                }
                break;
        }
        

        return tappedInZone;
    }

    public void handleInGreenZone(float currentTime, TimeZones _zone = null)
    {

        switch (spawnType)
        {
            case ZoneSpawnType.ZoneList:
            case ZoneSpawnType.ZoneRndList:
                for (int i = 0; i < goalZones.Count; i++)
                {
                    if (currentTime >= goalZones[i].startTime && currentTime <= (goalZones[i].startTime + goalZones[i].duration) && goalZones[i].isArchived == false)
                    {
                        //handle.sprite = idealZoneSprite;
                        sliderImage.color = Color.green;
                        break;
                    }
                    else
                    {
                        // handle.sprite = commonZoneSprite;
                        sliderImage.color = Color.white;
                    }
                }

                break;

            case ZoneSpawnType.ZoneRndSingle:
                if (currentTime >= _zone.startTime && currentTime <= (_zone.startTime + _zone.duration) && _zone.isArchived == false)
                {
                    //handle.sprite = idealZoneSprite;
                    sliderImage.color = Color.green;
                    break;
                }
                else
                {
                    // handle.sprite = commonZoneSprite;
                    sliderImage.color = Color.white;
                }
                break;
        }

        
    }

    public IEnumerator HammerBashEffect(float _bashType)
    {
        int bashType = 0;
        if (_bashType >= 0 && _bashType < hitForcePercent.x)
            bashType = 1;
        if (_bashType >= hitForcePercent.x && _bashType < hitForcePercent.y)
            bashType = 2;
        if (_bashType >= hitForcePercent.y)
            bashType = 3;

        while (true)
        {
            Debug.Log("Hammer Anlge:" + hammer.gameObject.transform.eulerAngles.x);
            if (hammer.gameObject.transform.eulerAngles.x >= -1)
            {
                //Play effect, destroy cour
                var em = hammerHitEffect.emission;

                switch (bashType)
                {
                    case 1:
                        em.rateOverTime = 20f;
                        break;

                    case 2:
                        em.rateOverTime = 40f;
                        break;

                    case 3:
                        em.rateOverTime = 80f;
                        break;
                }

                //hammerHitEffect.Play();
                bashEffect = null;

                if (progress == goal)
                {
                    
                    mechInit = false;
                    EmogiNtextPopup.singleton.FullPopupSpawn();
                    StepsManager.singleton.AddProgress();

                    yield return new WaitForSeconds(2.5f);

                    
                    PolishTaskManager.singleton.MechInit();
                    tapForceSlider.gameObject.SetActive(false);
                    progressTxt.gameObject.SetActive(false);
                    
                }

                yield break;
            }

           
            yield return null;
        }
    }
}

public enum ZoneSpawnType
{
    ZoneList, //Zones spawns and check from list(use zone list)
    ZoneRndList, //Create zone on slider with rnd pos and duration(overwrite zone list)
    ZoneRndSingle, //Clear zone list, create one zone on slider
}