﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public float smoothTime = 0.3f;
    public Vector3 velocity = Vector3.zero;
    public float cameraDistanceMultiplier = 1;
    public float cameraYMultiplier = 1;
    // Update is called once per frame
    void Update()
    {
        if (target == null)
            return;

        Vector3 targetPos = target.TransformPoint(new Vector3(-6.51f / cameraDistanceMultiplier, 3.65f / cameraDistanceMultiplier * cameraYMultiplier, -5.8f / cameraDistanceMultiplier));

        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, smoothTime);
        transform.LookAt(target);
    }
}
